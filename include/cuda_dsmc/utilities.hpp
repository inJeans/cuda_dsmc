#ifndef UTILITIES_HPP_INCLUDED
#define UTILITIES_HPP_INCLUDED 1

void progress_bar(int current,
                  int total);

#endif  // UTILITIES_HPP_INCLUDED
