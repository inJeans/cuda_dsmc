/**
 *  More detailed description
 *  Copyright 2015 Christopher Watkins
 */

#ifndef COLLISIONS_TEST_HPP_INCLUDED
#define COLLISIONS_EVOLUTION_TEST_HPP_INCLUDED 1

#include <cuda_runtime.h>
#include "cublas_v2.h"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "distribution_generation.hpp"
#include "collisions.hpp" 
#include "vector_math.cuh"
#include "utilities.hpp"

#include "define_host_constants.hpp"

#endif  // COLLISIONS_EVOLUTION_TEST_HPP_INCLUDED