[![Build Status](https://travis-ci.org/inJeans/cuda_dsmc.svg)](https://travis-ci.org/inJeans/cuda_dsmc)
[![Build status](https://ci.appveyor.com/api/projects/status/kdmwtlq123imd1o3?svg=true)](https://ci.appveyor.com/project/inJeans/cuda-dsmc)
[![Coverage Status](https://coveralls.io/repos/inJeans/cuda_dsmc/badge.svg?branch=master&service=github)](https://coveralls.io/github/inJeans/cuda_dsmc?branch=master)
[![Documentation Status](https://readthedocs.org/projects/cuda-dsmc/badge/?version=latest)](http://cuda-dsmc.readthedocs.org/en/latest/?badge=latest)
[![Project Stats](https://www.openhub.net/p/cuda_dsmc/widgets/project_thin_badge.gif)](https://www.openhub.net/p/cuda_dsmc)
                

# cuda_dsmc
CUDA based implementation of G A Bird's DSMC algorithm.
